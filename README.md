# GoSolo
GoSolo is safe and simple way to get into a solo public session of Grand Theft Auto Online written in Go. GoSolo simply suspends the `GTA5` process for 10 seconds and then resumes it. If run on Windows, GoSolo will attempt to bring the Grand Theft Auto window back into focus as a convenience. **GoSolo does nothing else and does not modify or inject any code into the game.** It really is that straightforward.

## Requirements
- Grand Theft Auto 5

## Usage
Run the binary any time you're in a session with others and just need to get away.

## Contributing
Merge requests are welcome after opening an issue first.
