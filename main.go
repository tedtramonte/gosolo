package main

import (
	"context"
	"log"
	"os"
	"strings"
	"time"

	"github.com/shirou/gopsutil/v3/process"
)

func main() {
	const procTitle = "GTA5"
	const windowTitle = "Grand Theft Auto V"

	var gtav *process.Process

	log.Println("Enumerating processes...")
	processes, _ := process.Processes()

	for i := 0; i < len(processes); i++ {
		name, _ := processes[i].Name()
		if strings.Contains(name, procTitle) {
			log.Println("Process found.")
			FocusWindow(windowTitle)
			gtav = processes[i]
			log.Println("Suspending process...")
			gtav.SuspendWithContext(context.Background())
			time.Sleep(10 * time.Second)
			gtav.ResumeWithContext(context.Background())
			log.Println("Process resumed.")
			os.Exit(0)
		}
	}

	log.Fatalln("Process not found")
}
