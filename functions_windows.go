package main

import (
	"log"
	"strings"
	"syscall"

	"github.com/gonutz/w32/v2"
	"github.com/hnakamur/w32syscall"
)

func FocusWindow(title string) {
	var focused bool

	log.Println("Focusing window...")
	callback := func(hwnd syscall.Handle, lparam uintptr) bool {
		h := w32.HWND(hwnd)
		_text := w32.GetWindowText(h)
		if strings.Contains(_text, title) {
			focused = w32.SetForegroundWindow(h)
		}
		return true
	}

	err := w32syscall.EnumWindows(callback, 0)
	if err != nil {
		log.Fatalln(err)
	}
	if focused {
		log.Println("Window focused")
	} else {
		log.Println("Window not found")
	}
}
